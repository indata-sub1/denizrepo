﻿using HareketApi.Application;
using HareketApi.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace HareketApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class HareketController : ControllerBase
    {
        private readonly IHareketService _hareketService;
        public HareketController(IHareketService hareketService)
        {
                _hareketService = hareketService;
        }

        [HttpPost]
        [Route("/v1/hareket/start")]
        public async Task<IActionResult> StartAsync([FromBody]HareketInputModel input)
        {
            await _hareketService.StartAsync(input);
            return Ok(true);
        }

        [HttpPost("/v1/hareket/pause")]
        public async Task<IActionResult> PauseAsync([FromBody] HareketInputModel input)
        {
            await _hareketService.PauseAsync(input);
            return Ok(true);
        }

        [HttpPost]
        [Route("/v1/hareket/stop")]
        public async Task<IActionResult> StopAsync([FromBody] HareketInputModel input)
        {
            await _hareketService.StopAsync(input);
            return Ok(true);
        }
    }
}
