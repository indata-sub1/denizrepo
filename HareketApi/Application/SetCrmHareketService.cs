﻿using HareketApi.Models;
using SetCRMHelper;
using SetCRMHelper.MayaModels;

namespace HareketApi.Application
{
    public class SetCrmHareketService : IHareketService
    {
        private readonly ISetCrmHelper _setCrmHelper;
        public SetCrmHareketService(ISetCrmHelper setCrmHelper)
        {
            _setCrmHelper = setCrmHelper;
        }

        /*
            {
                "CustomObjectId" : "57FA4F51296740368BABB61E1B16AB3F",
                "FieldsValues" :{
                    "F3B2C3D4E9954BC4A30AA05EA474EC36": "2022.03.14T05:00:00",
                    "D6CCE06981234A5CA4D83E78FECF18CF": "AC6CAE0D2D9B4993AC5C418370898CFF",
                    "7DC019A709A349A682D805B61D52136C": "902CB693AEFA411B9238DE5724291227"
                }
            }
         */
        public async Task StartAsync(HareketInputModel input)
        {
            var apiRequestModel = new RecordRequestParameters()
            {
                CustomObjectId = "57FA4F51296740368BABB61E1B16AB3F",
                IsForcingSave = false,
                FieldsValues = new Dictionary<string, object>()
            };
            apiRequestModel.FieldsValues.Add("F3B2C3D4E9954BC4A30AA05EA474EC36", DateTime.Now);//.ToString("yyyy.MM.ddTHH:mm:ss")
            apiRequestModel.FieldsValues.Add("D6CCE06981234A5CA4D83E78FECF18CF", input.HareketUserId);
            apiRequestModel.FieldsValues.Add("7DC019A709A349A682D805B61D52136C", "902CB693AEFA411B9238DE5724291227");

            await _setCrmHelper.PostRecordAsync(apiRequestModel);
        }

        public async Task PauseAsync(HareketInputModel input)
        {
            var apiRequestModel = new RecordRequestParameters()
            {
                CustomObjectId = "57FA4F51296740368BABB61E1B16AB3F",
                IsForcingSave = false,
                FieldsValues = new Dictionary<string, object>()
            };
            apiRequestModel.FieldsValues.Add("F3B2C3D4E9954BC4A30AA05EA474EC36", DateTime.Now);
            apiRequestModel.FieldsValues.Add("D6CCE06981234A5CA4D83E78FECF18CF", input.HareketUserId);
            apiRequestModel.FieldsValues.Add("7DC019A709A349A682D805B61D52136C", "714FBEA76132468289622A622D9FAB05");

            await _setCrmHelper.PostRecordAsync(apiRequestModel);
        }


        public async Task StopAsync(HareketInputModel input)
        {
            var apiRequestModel = new RecordRequestParameters()
            {
                CustomObjectId = "57FA4F51296740368BABB61E1B16AB3F",
                IsForcingSave = false,
                FieldsValues = new Dictionary<string, object>()
            };
            apiRequestModel.FieldsValues.Add("F3B2C3D4E9954BC4A30AA05EA474EC36", DateTime.Now);
            apiRequestModel.FieldsValues.Add("D6CCE06981234A5CA4D83E78FECF18CF", input.HareketUserId);
            apiRequestModel.FieldsValues.Add("7DC019A709A349A682D805B61D52136C", "9BDB893E92AF41398757D483F27C447A");

            await _setCrmHelper.PostRecordAsync(apiRequestModel);
        }
    }
}
