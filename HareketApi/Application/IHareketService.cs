﻿using HareketApi.Models;

namespace HareketApi.Application
{
    public interface IHareketService
    {
        Task StartAsync(HareketInputModel input);
        Task PauseAsync(HareketInputModel input);
        Task StopAsync(HareketInputModel input);
    }
}
