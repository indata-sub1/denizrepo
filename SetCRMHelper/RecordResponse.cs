﻿using System.ComponentModel.DataAnnotations;

namespace SetCRMHelper.MayaModels
{
    public class RecordResponse
    {
        public string RecordId { get; set; }
        public List<ValidationResult> Errors { get; set; }
        public List<ValidationResult> Warnings { get; set; }
        public List<ValidationResult> Informations { get; set; }
        public bool IsOk { get; set; }
        public string Message { get; set; }
    }
}
