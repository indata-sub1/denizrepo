﻿using SetCRMHelper.MayaModels;

namespace SetCRMHelper
{
    public interface ISetCrmHelper
    {
        Task<RecordResponse> PostRecordAsync(RecordRequestParameters input);
    }
}
