﻿using RestSharp;
using SetCRMHelper.MayaModels;
using System.Net.Http.Headers;
using System.Text;
using System.Text.Json;

namespace SetCRMHelper
{
    public class SetCrmHelper : ISetCrmHelper
    {
        public async Task<RecordResponse> PostRecordAsync(RecordRequestParameters input)
        {
            //string response = "";
            //using (var client = new HttpClient())
            //{
            //    HttpContent contentPost = new StringContent(JsonSerializer.Serialize(input), Encoding.UTF8, "application/json");
            //    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("62C01F60A209455CB4C1CFE6F55A5471");
            //    var request = await client.PostAsync("https://demoevimapi.indata.com.tr/v1/record", contentPost);
            //    response = await request.Content.ReadAsStringAsync();
            //}
            //try
            //{
            //    var result = JsonSerializer.Deserialize<RecordResponse>(response);
            //    return result;
            //}
            //catch (Exception ex)
            //{
            //    throw new Exception($"API Post Record Failed! [REQUEST:{JsonSerializer.Serialize(input)}]");
            //}

            var client = new RestClient("https://demoevimapi.indata.com.tr/v1/record");
            client.Timeout = -1;
            var request = new RestRequest(Method.POST);
            request.AddHeader("Authorization", "62C01F60A209455CB4C1CFE6F55A5471");
            request.AddHeader("Content-Type", "application/json");
            var body = JsonSerializer.Serialize(input);
            request.AddParameter("application/json", body, ParameterType.RequestBody);
            var response = client.Execute(request);

            if (response.IsSuccessful)
            {
                var result = JsonSerializer.Deserialize<RecordResponse>(response.Content);
                return result;
            }
            else
            {
                throw new Exception($"API Post Record Failed! [REQUEST:{body}]");
            }
        }
    }
}
